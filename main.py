from typing import Any
from numpy.typing import NDArray
import pandas
import json
import logging
import numpy as np

from sklearn import svm
from sklearn.preprocessing import OrdinalEncoder, LabelEncoder, StandardScaler
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import GridSearchCV

from argparse import ArgumentParser

BIAS_NONE_BIAS_INDICATORS = ["nevertheless", "despite this", "on top of this", "on other side", "admittedly", "undoubtedly , everything has two sides",
                             "it is also worth mentioning", "looking from another perspective ,", "another debatable",
                             "another perspective", "on the other hand", "some may argue", "some people", "however ,", "finally , some", "but on other hand",
                             ]

# parser for arguments -v and -t indicating if the model should be tested or validated or both
PARSER = ArgumentParser()
PARSER.add_argument("-v", "--validate", default=True, help="if validate is set 5-fold cross validation is performed befor fitting the model", action="store_true")
PARSER.add_argument("-t", "--test", default=False, help="if test is set, the model is tested on the test file", action="store_true")
PARSER.add_argument("-d", "--debug", default=False, help="if debug is set, the logger level will be set to debug", action="store_true")
PARSER.add_argument("-o", "--optimization", default=False, help="if optimization is set, hyperparameter optimization is performed", action="store_true")

# standard logger with german dateformat
logging.basicConfig(format='%(asctime)s %(message)s', datefmt='%d.%m.%Y %H:%M:%S', level=logging.INFO)
LOGGER = logging.getLogger('Main_logger')


def save_predictions_as_json(filename: str, data: list, predictions: list):
    
    LOGGER.info(f"Saving predictions to file {filename}")
    
    # get essay id and corresponsing preditions
    predictions_formated = [{"id": zip_data_prediction[0]["id"], "confirmation_bias": bool(zip_data_prediction[1])} for zip_data_prediction in zip(data, predictions)]
    # save predictions in json file
    with open(filename, 'w') as f:
        json.dump(predictions_formated, f)
        
    LOGGER.info(f"Successfully saved predictions to file {filename}")


def load_csv_data(filename: str, row_limit: int|None=None) -> pandas.DataFrame | None:
    
    LOGGER.info(f"Loading file {filename}")
    
    df = None
    try: 
        df = pandas.read_csv(filename, sep=";", engine="python")
    except FileNotFoundError: 
        LOGGER.warning(f"Could not find file {filename}, no file was loaded")
    else:
            
        LOGGER.info(f"Successfully loaded file {filename}")
        
        # check if dataframe should be limited
        if row_limit is not None and row_limit < len(df): df = df[:row_limit]
        # replace word "essay" befor every id with nothing
        df["ID"] = df["ID"].str.replace("essay","")
        df.set_index("ID", drop=True, inplace=True)
    finally: 
        return df


def load_json_data(filename: str) -> dict|None:
    
    LOGGER.info(f"Loading file {filename}")
    
    json_data = None
    
    # try opening file with name filename
    try: 
        with open(filename, encoding="utf8") as f: json_data = json.load(f)
    except FileNotFoundError: LOGGER.warning(f"Could not find file {filename}, no file was loaded")
    else: LOGGER.info(f"Successfully loaded file {filename}")
    # return json_data (might be None)
    finally: return json_data
 
 
def split_json_data(json_data: dict, split_df: pandas.DataFrame) -> tuple[list, list]:
    train_list = list()
    test_list = list()
    
    # for every entry in json data 
    for json_datum in json_data:
        # get essay id and convert it to string with 3 digits
        essay_id = f"{json_datum['id']:03}"
        # check if essay is in training or test set
        membership = split_df.loc[essay_id]["SET"]
        # save essay data in appropriate dict
        if membership == "TEST": test_list.append(json_datum)
        else: train_list.append(json_datum)
    # return two dicts containing training- and test-data
    return train_list, test_list


def feature_from_essay(essay: dict) -> list: 
    non_bias_indicator_count = [0 for _ in range(len(BIAS_NONE_BIAS_INDICATORS))]
    # check if any paragraph starts with a non biased inicatore
    for paragraph in essay["paragraphs"]:
        # text of paragraph
        text = paragraph["text"].lower()
        # check if paragraph starts with non biased inicatore
        for index, phrase in enumerate(BIAS_NONE_BIAS_INDICATORS):
            non_bias_indicator_count[index] += sum([phrase in text[:2 * len(phrase)] ])
            
    print(non_bias_indicator_count)
    
    # construct feature vector
    feature_vector = list()
    feature_vector.extend(non_bias_indicator_count)

    LOGGER.debug(f"{essay['confirmation_bias']=}, {feature_vector=}")
    
    return feature_vector


def get_target_values_and_feature_vectors(data: list) -> tuple[list, list]:

    LOGGER.info(f"Generating feature vectors and target values")

    feature_vectors = list()
    target_values = list()
    
    # for every essay in data
    for essay in data:
        # generate feature vector of essay
        feature_vectors.append(feature_from_essay(essay))
        # target_values is, if the essay has confirmation_bias
        target_values.append(essay["confirmation_bias"])
    
    
    LOGGER.info(f"Successfully generated feature vectors and target values")
    
    return feature_vectors, target_values
    

def train_model(model: svm.SVC, feature_vectors: list, target_values: list, label_enc: LabelEncoder, original_enc: OrdinalEncoder, scaler: StandardScaler):
    
    LOGGER.info(f"Begin training process")
    
    # encode feature vector and target_values
    encoded_feature_vectors = original_enc.fit_transform(feature_vectors)
    scaled_encoded_feature_vectors = scaler.fit_transform(encoded_feature_vectors)
    encoded_target_values = label_enc.fit_transform(target_values)
    
    # make sure the vectors are numpy arrays
    X = np.array(scaled_encoded_feature_vectors)
    y = np.array(encoded_target_values)
    # fit model on whole training data set
    model.fit(X, y)
    
    LOGGER.info(f"Successfully completed training process")
    
    
def validate_model(model: svm.SVC, feature_vectors: list, target_values: list, label_enc: LabelEncoder, original_enc: OrdinalEncoder, scaler: StandardScaler):
    
    LOGGER.info(f"Begin validation process")
    
    # encode feature vector and labels
    encoded_feature_vectors = original_enc.fit_transform(feature_vectors)
    scaled_encoded_feature_vectors = scaler.fit_transform(encoded_feature_vectors)
    encoded_target_values = label_enc.fit_transform(target_values)
    
    # make sure the vectors are numpy arrays
    X = np.array(scaled_encoded_feature_vectors)
    y = np.array(encoded_target_values)
    
    LOGGER.info(f"Start 5-fold cross validation process")
    
    # 5-fold cross validation
    scores = cross_val_score(model, X, y, cv=5, n_jobs=5, scoring='f1')
    
    LOGGER.info(f"Successfully completed 5-fold cross validation")
    LOGGER.info(f"Model achieved an average F1-score of {scores.mean():{1}.{4}f} with a standard deviation of {scores.std():{1}.{4}f}")
    LOGGER.info(f"Successfully completed vaidation process")
    
    return scores


def generate_svm_with_hyperparameter_optimization(feature_vectors: list, target_values: list, label_enc: LabelEncoder, original_enc: OrdinalEncoder, scaler: StandardScaler) -> svm.SVC:

    LOGGER.info(f"Generating model with optimized hyperparameters")

    # support vector maschine model
    model = svm.SVC()
    
    LOGGER.info(f"Successfully created maschine learning model")
    
    # encode feature vector and labels
    encoded_feature_vectors = original_enc.fit_transform(feature_vectors)
    scaled_encoded_feature_vectors = scaler.fit_transform(encoded_feature_vectors)
    encoded_target_values = label_enc.fit_transform(target_values)
    
    # make sure the vectors are numpy arrays
    X = np.array(scaled_encoded_feature_vectors)
    y = np.array(encoded_target_values)
    
    # hyperparameters   
    parameters = [
                  {'C': [1, 10, 100], 'kernel': ['linear', 'poly', 'rbf', 'sigmoid'], 'degree': [1,2,3,4]},
                 ]
    
    # grid search for best hyperparameters
    clf = GridSearchCV(model, parameters, n_jobs=24)
    clf.fit(X, y)
    
    LOGGER.info(f"Successfully completed generation of model with optimized hyperparameters")
    
    # return resulting model with optimized hyperparameters
    return model
    
def test_model(model: svm.SVC, feature_vectors: list, label_enc: LabelEncoder, original_enc: OrdinalEncoder, scaler: StandardScaler) -> list:
    
    LOGGER.info(f"Begin test process")
    
    # encode feature vector
    encoded_feature_vectors = original_enc.fit_transform(feature_vectors)
    scaled_encoded_feature_vectors = scaler.fit_transform(encoded_feature_vectors)
    
    # make sure the vector is a numpy array
    X = np.array(scaled_encoded_feature_vectors)
    # get predictions by using the model and reverse transforming the result
    predictions = label_enc.inverse_transform(model.predict(X))
    
    LOGGER.info(f"Successfully completed test process")
    
    if predictions == None: return list()
    return list(predictions)   
    
    
def main(validate: bool, test: bool, debug: bool, optimization: bool):
    
    # set mode to debug mode if debug is true
    if debug: LOGGER.setLevel(logging.DEBUG)
    
    # load csv file as pandas dataframe containing train test split
    mapping_file_name = 'data/train-test-split.csv'
    split_df = load_csv_data(mapping_file_name, row_limit=None)
      
    if split_df is None: return
    
    # load essay data from json file
    data_file_name = "data/essay-corpus.json"
    json_data = load_json_data(data_file_name)

    if json_data is None: return
    
    # get split of training and test data according to split_df
    train_data, test_data = split_json_data(json_data, split_df)
    
    # feature vectors and target values of training data
    feature_vectors, target_values = get_target_values_and_feature_vectors(train_data)
    
    # check that number of target values is equal to number of feature_vectors
    if len(feature_vectors) != len(target_values):
        logging.error(f"Number of essays {len(feature_vectors)} seems to be not equal to number of evaluations {target_values}")
        return
    
    # encoders to encode strings as numbers
    label_enc = LabelEncoder()
    original_enc = OrdinalEncoder()
    # scaler scaling numbers between 0.0 and 1.0
    scaler = StandardScaler()  
    
    # support vector maschine
    csf = svm.SVC()
    
    if optimization:
        csf = generate_svm_with_hyperparameter_optimization(feature_vectors, target_values, label_enc, original_enc, scaler)
    
    if validate:
        validate_model(csf, feature_vectors, target_values, label_enc, original_enc, scaler)
    if test:
        train_model(csf, feature_vectors, target_values, label_enc, original_enc, scaler)
        # feature vectors and target values of test data
        feature_vectors, _ = get_target_values_and_feature_vectors(test_data)
        # generate predictions and save them to predictions.json file
        predictions = test_model(csf, feature_vectors, label_enc, original_enc, scaler)
        save_predictions_as_json('predictions.json', test_data, predictions)

  
if __name__ == '__main__':
    args = vars(PARSER.parse_args())
    # check that at least one argument is set
    if not args["validate"] and not args["test"]: PARSER.error("at least one of --validate and --test is required")
    else: main(args["validate"], args["test"], args["debug"], args["optimization"])
    